import numpy as np
from matplotlib import pyplot as plt
import time
from multiprocessing import Pool, cpu_count
import subprocess
import sys

def runner(arg):
    ITER = 50000
    length = 400
    n_bikes = arg
    n_cb = 5
    max_speed = 5
    subprocess.run(f"python class_bikes_analysis.py {ITER} {length} {n_bikes} {n_cb} {max_speed}", shell = True)

if __name__ == '__main__':
    pool = Pool(cpu_count())
    #res = pool.map(runner, [100])
    res = pool.map(runner, np.arange(30,700, 10))
    #res = pool.map(runner, (np.ones(10)*200).astype(int))
    pool.close()
    pool.join()
