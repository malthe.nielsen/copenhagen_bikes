import numpy as np
from matplotlib import pyplot as plt
import time
from multiprocessing import Pool, cpu_count
import subprocess
import sys

def runner(arg):
    ITER = 50000
    length = 400
    n_bikes = 400
    n_cb = 5
    max_speed = 5
    randomness = arg / 100
    subprocess.run(f"python do_simulation.py --n_iterations {ITER}
            --road_length {length}
            --n_bikes {n_bikes}
            --n_cbikes{n_cb}
            --max_speed {max_speed}
            --ran {randomness}", shell = True)

if __name__ == '__main__':
    pool = Pool(cpu_count())
    #res = pool.map(runner, [100])
    res = pool.map(runner, np.arange(1,50,1))
    #res = pool.map(runner, (np.ones(10)*200).astype(int))
    pool.close()
    pool.join()
