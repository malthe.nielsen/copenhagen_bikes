import numpy as np
from matplotlib import pyplot as plt
from collections import Counter
import re


#  road = np.random.randint(0,2,20).reshape(2,10)
#  road_2 = np.random.randint(0,4,20).reshape(2,10)*road
#  ids = np.arange(1,21,1).reshape(2,10)*road
#  road = np.dstack((road, road_2))
#  road = np.dstack((road, ids))
#  print(road[:,:,0])
#  print(road[:,:,1])
#  print(road[:,:,2])


def scanner(road,start,max_speed, queue):
    N = 0
    for i in range(0,max_speed):
        for j in range(2):
            index = start - i
            ID = road[j, index, -1]
            if ID in(queue):
                continue
            if index < 0:
                index = road.shape[1] + index
            if road[j,index,0] > 0 and road[j,index,1] >= i:
                N += 1
                queue.insert(0,ID)
                queue.pop()

    return N, queue

def flux_in_time(road):
    max_speed = 4
    queue = list(np.zeros(max_speed*2))
    start = 0
    flux = []
    for i in range(road.shape[-1]):
    #  for i in range(5):
        N, queue = scanner(road[...,i],start,max_speed, queue)
        flux.append(N)
        print(N)
        print(queue)
        start -= 1
        if start < 0:
            start = road.shape[1] + start
    return np.mean(flux)

def flux_in_time(road):
    max_speed = 10
    start = 100
    back = np.zeros((2,max_speed)).ravel()
    back_twice = np.zeros_like(back)
    
    flux = []
    old_IDS = [0]
    for i in range(road.shape[-1]):
        front = road[:,start-max_speed:start,4,i].ravel()
        #  front_and_back = np.hstack([front.ravel(), back.ravel()])
        #  unq = []
        FLUX = 0
        for b in back:
            if b not in front and b != 0:
                FLUX += 1
        
        #  for item, count in Counter(front_and_back).most_common():
        #      if count == 1 and item != 0:
        #          unq.append(item)
        #  FLUX = len(unq)
        #  for i in range(unq):
        #      if not unq in(old_IDS):
        #          FLUX -= 1
        flux.append(FLUX)
        back = front.copy()
        #  back_twice = back.copy()
    return flux


        



