import numpy as np
from matplotlib import pyplot as plt
from matplotlib import patches
import sys
import glob
import tqdm
import re
from multiprocessing import Pool, cpu_count



fnames = glob.glob('/run/media/nordentoft/backup/data/*.npy')
dat = []
dat_ran = []
for fname in fnames:
    if re.search('_hist', fname) != None:
        dat.append(fname)
        print(fname)
        ran = float(fname.split('=')[-1][:-9])
        dat_ran.append(ran)


dat = np.array(dat)
dat = dat[np.argsort(dat_ran)]

image = np.zeros((100,len(dat)))



for i, data in enumerate(dat):
    image[:,i] = np.load(data)


fig, ax = plt.subplots(1,2, figsize = (10,5))
ax[0].imshow(np.flipud(image[:,:]), aspect = 'auto', extent = [0,0.5,0,4], cmap = 'coolwarm')
ax[0].set_xlabel('Randomness')
ax[0].set_ylabel('Speed')

for i in range(3,image.shape[1]):
    #  plt.bar(np.linspace(0,4,100, endpoint = True), img[:,i])
    ax[1].plot(np.linspace(0,4,100,endpoint = True), image[:,i] + 1*i, color = 'black', alpha = 0.3)
ax[1].set_ylabel('Randomness [AU]')
ax[1].set_xlabel('Speed1')


plt.show()
