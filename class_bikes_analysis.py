import numpy as np
from matplotlib import pyplot as plt
from matplotlib import patches
import sys
import glob

def func(road, val):
    time_since = 0
    avg_speed = []
    avg_time = []
    passed = False
    desired_speed = 0
    for i in range(road.shape[-1]):
        if (road[:, 100:103,4,i] == val).any():
            passed = True
            if desired_speed == 0:
                for k in range(100,103):
                    for j in range(2):
                        if road[j,k,4,i] == val:
                            desired_speed = road[j,k,1,i]
        if (road[:,10:13, 4, i] == val).any():
            if passed == True:
                avg = 400/(i - time_since)
                avg_speed.append(avg)
                avg_time.append(i)
                time_since = i
                passed = False

    return avg_speed, avg_time, desired_speed

def loader(road, N_bikes):
    speed_1 = []
    speed_2 = []
    speed_3 = []
    speed_4 = []
    for bike_ID in range(1,int(N_bikes)):
        avg_speed, avg_time, desired_speed = func(road, bike_ID)
        print(bike_ID, desired_speed)
        if desired_speed == 1:
            speed_1.append(avg_speed)
        elif desired_speed == 2:
            speed_2.append(avg_speed)
        elif desired_speed == 3:
            speed_3.append(avg_speed)
        else:
            speed_4.append(avg_speed)

    val = np.zeros((100))
    if len(speed_1) > 0:
        speed_1 = np.hstack(speed_1)
        val1, _ = np.histogram(speed_1, bins = np.linspace(0,4,101, endpoint = True), density = True)
        val += val1
    if len(speed_2) > 0:
        speed_2 = np.hstack(speed_2)
        val2, _ = np.histogram(speed_2, bins = np.linspace(0,4,101, endpoint = True), density = True)
        val += val2
    if len(speed_3) > 0:
        speed_3 = np.hstack(speed_3)
        val3, _ = np.histogram(speed_3, bins = np.linspace(0,4,101, endpoint = True), density = True)
        val += val3
    if len(speed_4) > 0:
        speed_4 = np.hstack(speed_4)
        val4, _ = np.histogram(speed_4, bins = np.linspace(0,4,101, endpoint = True), density = True)
        val += val4
    #avg_speed = np.hstack(speed_bin)
    #val, _ = np.histogram(avg_speed, bins = np.linspace(0,4,100, endpoint = True), density = True)
    return val, N_bikes
colour_key = np.array([255/301, 3/301, 43/301])

def colourizer(risk_factor, rgb_value = np.array([255/301, 3/301, 43/301])):
    return [((rgb_value*risk_factor))]

length_of_road = int(sys.argv[2])
road = np.zeros((2,length_of_road,5))

'''
0 = street level, which types
1 = desired speed
2 = actual speed
3 = dare
4 = ID

'''
number_of_bikes = int(sys.argv[3])
#
#N_bikes = np.random.randint(0,length_of_road, number_of_bikes)
N_bikes = np.random.choice(np.arange(0,length_of_road*2,1), number_of_bikes, replace = False)
N_lane  = np.zeros(number_of_bikes)

for i in range(number_of_bikes):
    if N_bikes[i] >= length_of_road:
        lane = 1
        N_bikes[i] -= (length_of_road + 1)
    else:
        lane = 0
    N_lane[i] = lane

for i in range(len(N_bikes)):
    dare = np.random.normal(.5,.5,1)[0]
    if dare < 0:
        dare = 0
    if dare > 1:
        dare = 1
    road[int(N_lane[i]), int(N_bikes[i]), :] = [2, np.random.randint(2,int(sys.argv[5]),1)[0], 1, dare, 0]
    #for k in range(2):
        #road[k, i, :] = [2, np.random.randint(1,int(sys.argv[5]),1)[0], 1, np.random.uniform(0,1,1)[0], 0]

CB_bikes = np.random.randint(0,length_of_road, int(sys.argv[4]))
for bike in CB_bikes:
    road[0,bike,:] = [1, 0, 0 ,0, 0]

counter = 1
for j in range(length_of_road):
    for i in range(2):
        if road[i,j,0] > 1:
            road[i,j,4] = counter
            counter += 1
print('COUNTER', counter, number_of_bikes)

#  def save_constructer(previously, current):
#      if len(previously.shape) != 4:
#          previously = np.expand_dims(previously, 3)
#      current = np.expand_dims(current, 3)
    #  return np.concatenate((previously, current), axis = 3)

def save_constructer(iterations, road):
    return np.zeros((road.shape[0], road.shape[1], road.shape[2], iterations))

def p_sight(road, speed, x, y):
    not_legal = True
    while not_legal:
        jump = int(y+speed+1)

        if jump > road.shape[1]:
            road_tmp_front = [road[x,y+1:,0], road[x,:jump%road.shape[1],0]]
            road_tmp_front = np.hstack(road_tmp_front)
            
            road_tmp_side = [road[(x+1)%2,y:,0], road[(x+1)%2,:jump%road.shape[1],0]]
            road_tmp_side = np.hstack(road_tmp_side)

            front = np.any(road_tmp_front != 0)
            side  = np.any(road_tmp_side != 0)
            jump = jump%road.shape[1]
            #  print(jump)
            #  print(road_tmp_front, road_tmp_side, jump)
            #  print(road_tmp_front, 'front')
            #  print(road_tmp_side, 'side')
            #  print('front', front, 'side', side)
            #  print(road[x,:jump%road.shape[1]+1, 0])

        else:
            #  print('infront')
            #  print(road[x, y+1:jump,0])
            #  print(road[(x+1)%2, y:jump,0])

            front = np.any(road[x, y+1:jump, 0] != 0)
            side  = np.any(road[(x+1)%2, y:jump, 0] != 0)

        if front == False or side == False:
            not_legal = False
            if side == True: #we continue in our present lane
                return x, speed, jump
            elif side == False and front == False:
                if x == 1:
                    return (x+1)%2 , speed, jump
                else:
                    return x, speed, jump
            else:
                #  print('I chose to switch it up')
                #  print((x+1)%2 , speed, jump)
                return (x+1)%2 , speed, jump
        else:
            speed -= 1
        if speed <= 0:
            not_legal == False
            return x, 0, 0

def p_lane_switch(road, speed, x, y):
    not_legal = True
    while not_legal:
        jump = int(y+speed+1)

        if jump > road.shape[1]:
            road_tmp_front = [road[x,y+1:,0], road[x,:jump%road.shape[1],0]]
            road_tmp_front = np.hstack(road_tmp_front)
            
            road_tmp_side = [road[(x+1)%2,y:,0], road[(x+1)%2,:jump%road.shape[1],0]]
            road_tmp_side = np.hstack(road_tmp_side)

            front = np.any(road_tmp_front != 0)
            side  = np.any(road_tmp_side != 0)
            jump = jump%road.shape[1]

        else:
            front = np.any(road[x, y+1:jump, 0] != 0)
            side  = np.any(road[(x+1)%2, y:jump, 0] != 0)

        if front == False and side == False: 
            return (x+1)%2 , speed, jump

        if front == False or side == False:
            not_legal = False
            if side == True: #we continue in our present lane
                return x, speed, jump
            elif side == False and front == False:
                if x == 1:
                    return (x+1)%2 , speed, jump
                else:
                    return x, speed, jump
            else:
                return (x+1)%2 , speed, jump
        else:
            speed -= 1
        if speed <= 0:
            not_legal == False
            return x, 0, 0

def p_front(road, speed, x, y):
    not_legal = True

    while not_legal:
        jump = int(y+speed+1)

        if jump > road.shape[1]:
            road_tmp = [road[x,y+1:,0], road[x,:jump%road.shape[1],0]]
            road_tmp = np.hstack(road_tmp)
            front = np.any(road_tmp != 0)
            jump = jump%road.shape[1]

        else:
            front = np.any(road[x, y+1:jump, 0] != 0)

        if front == False:
            not_legal = False
            return x, speed, jump
        else:
            speed -= 1
        if speed <= 0:
            not_legal == False
            return x, 0, 0

def random_move_update(road, i, j):
    random_type = np.random.choice(['speed', 'lane'])
    if random_type == 'speed':
        speed = road[i, j, 2] - 1  
        if speed < 0:
            speed == 0

        x, new_speed, jump = p_lane_switch(road, speed, i, j)

        if new_speed <= 0:
            return road

        else:
           road[i,j,2] = new_speed
           road[int(x), int(jump - 1), :] = road[i, j, :]
           road[i,j,:] = 0
           return road

    if random_type == 'lane':
        speed = road[i, j, 2] 
        x, new_speed, jump = p_sight(road, speed, i, j)
        if x == 1 and i == 0: #Check to see if we dare overtake
            if np.random.uniform(0,1,1)[0] > road[i,j,3]:
                x, new_speed, jump = p_front(road, speed, i, j)
        if new_speed == 0:
            return road

        else:
           road[i,j,2] = new_speed
           road[int(x), int(jump - 1), :] = road[i, j, :]
           road[i,j,:] = 0
           return road
        
def updater(road, i, j):
    speed = road[i, j, 2]  
    if speed != road[i, j, 1]:
        speed += 1
    elif speed == 0:
        return road
    #  print('speed', speed)
    #initial attempt
    x, new_speed, jump = p_sight(road, speed, i, j)
    if x == 1 and i == 0: #Check to see if we dare overtake
        if np.random.uniform(0,1,1)[0] > road[i,j,3]:
            #  print('I did not dare', road[i,j,3])
            #  if true we did not dare to pull out and we stay behind for atleast one iteration
            x, new_speed, jump = p_front(road, speed, i, j)
        #      print('new')
        #  else:
        #      road[i,j,-1] += 1
        #      print('Going for the overtake')
        #      print('lane',x, 'speed',new_speed,'jump',jump)
    
    #  print('lane',x, 'speed',new_speed,'jump',jump)
    if new_speed <= 0:
        #  print('unlucky')
        #  print('lane',x, 'speed',new_speed,'jump',jump)
        return road

    else:
       road[i,j,2] = new_speed
       #  road[i,j,-2] += new_speed
       if road[int(x), int(jump-1),0] != 0:
           print(x, new_speed, jump - 1)
           print('problem')
           road[i,j,0] += 100
           road[int(x),int(jump-1),0] += 200
           print(jump - 1)
           print(road[:,:,0])
           sys.exit()
       road[int(x), int(jump - 1), :] = road[i, j, :]
       road[i,j,:] = 0
       return road


def random_picker(road):
    cyclists = np.where(road[:,:,0] > 1)
    N_cyclists = cyclists[0].shape[0]
    chosen = np.random.choice(np.arange(0,cyclists[0].shape[0],1), np.random.poisson(N_cyclists//10,1), replace = False)
    #  chosen = np.random.choice(np.arange(0,cyclists[0].shape[0],1), 4, replace = False)
    #  print(chosen)
    return (cyclists[0][chosen]),(cyclists[1][chosen])

def iterate(road):
    length = road.shape[1]
    rl, rp = random_picker(road)
    for j in range(length-1, -1, -1):
        for i in range(2-1, -1, -1):
            #if i in rl and j in rp:
                #if road[i,j,0] != 0:
                    #road = random_move_update(road, i, j)
                #  print(i,j)
                #  print(road[:,:,0])
                #  print('========================')
            #else:
            if road[i,j,0] != 0:
                road = updater(road, i, j)
                #  print(road[:,:,0])
                #  print('++++++++++++++++++++++++')
            #  else:
            #      continue
    return road

#  fig = plt.figure( figsize=(10,2)  )
#  fig, ax = plt.subplots(subplot_kw = {'projection':'polar'})
#  camera = Camera(fig)
#  ax = plt.Axes(fig, [0,0,1,1])
#  ax.set_axis_off()
#  fig.add_axes(ax)

def simmulation(iterations = 400, road = road):
    Sav = save_constructer(iterations, road)
    for i in (range(iterations)):
        road = iterate(road)
        Sav[...,i] = road

    val, N = loader(Sav, counter)
    np.save(f'./no_random/iter_{sys.argv[1]}_Length_{sys.argv[2]}_Nbike_{sys.argv[3]}_Ncb_{sys.argv[4]}_maxspeed_{sys.argv[5]}', val)
    #np.save(f'./data/iter_{sys.argv[1]}_Length_{sys.argv[2]}_Nbike_{sys.argv[3]}_Ncb_{sys.argv[4]}_maxspeed_{sys.argv[5]}', Sav)

simmulation(iterations = int(sys.argv[1]))

