import numpy as np
from matplotlib import pyplot as plt

def fn(p1, p2, pb = 0.05, rand = 0.1, D = 1):
    pt = p1+pb
    #  print(pt + p2)
    term_1 = (1-rand)*p1*((1-pt) + pt * D * (1 - p2)**2)
    term_2 = p1 *rand * (1 - p2)**2 
    term_3 = (1-rand)*p2*((1-p2)*(1-pt)**2 + (1 -p2)*pt)
    term_4 = p2 * rand * (1 - pt)**2
    return term_1 + term_2 + term_3 + term_4


img = np.zeros((100,80))
img_2 = np.zeros((100,80))

pcb = np.linspace(0,0.2,100)
p2  = np.linspace(0,0.5,80)

for i in range(100):
    for j in range(80):
        img[i,j] = (fn(0.8 - pcb[i] - p2[j], p2[j], pb = pcb[i]))

fig, ax = plt.subplots(1,1)
ax.imshow(img, cmap = 'inferno', extent = [0, 0.5, 0.2, 0], aspect = 'auto')
ax.set_ylabel('pcb')
ax.set_xlabel('p2')
plt.show()


img = np.zeros((100,80))
img_2 = np.zeros((100,80))

omega = np.linspace(0,0.2,100)
dare  = np.linspace(0,1,80)

for i in range(100):
    for j in range(80):
        img[i,j] = (fn(0.7 - omega[i], omega[i], 0.05, rand = 0.1, D = dare[j]))

fig, ax = plt.subplots(1,1)
ax.imshow(img, cmap = 'inferno', extent = [0, 0.5, 0.2, 0], aspect = 'auto')
ax.set_ylabel('Omega')
ax.set_xlabel('Dare')
plt.show()

