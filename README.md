# copenhagen_bikes

Small project where we try to simulate bike traffic using a NaSch approach mended to fit the behaviour of a cyclist in copenhagen

# Script overview

1. Analyzer.py: Takes an directory with analyzed average speed data, loads it into a numpy array and outputs this array as an .npy file. 

2. Imshow.py: Takes the file from the analyzer.py and plots it in the style we are used to. It could be an idea to merge the two scripts for our own sanity 

3. Multicore.py to be used when running scripts on ERDA. This creates a subprocess that runs a given simulation on one core of the processor. It is setup to use all cores - 1. The pool.map function does ***only*** function with np.int types

4. Class_bike_analysis.py The program that works from now, to be used from now on. Forst made as a copy to have a program that didnt save the entire simulation, but istead analyzed is as it runs. Here we find the average speed for one lap of the circle
